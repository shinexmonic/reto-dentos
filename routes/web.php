<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', [App\Http\Controllers\MainController::class, 'index'])->name('index');
Route::get('post/import', [App\Http\Controllers\PostController::class, 'import'])->name('importPosts');
Route::resource('post', App\Http\Controllers\PostController::class);

Route::get('get/posts/{user}', [App\Http\Controllers\PostController::class, 'getPostsApi'])->name('getPostsApi');