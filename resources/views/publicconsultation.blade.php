@extends('layouts.app')

@section('content')
<div class="container">
    <form action="{{ route('index') }}" method="get"> 
        <div class="input-group">
            <div class="col-5">
              <div class="input-group date" id="datepicker">
                <input type="text" class="form-control" autocomplete="off" name="created_at" id="date"/>
                <span class="input-group-append">
                  <span class="input-group-text bg-light d-block">
                    <i class="fa fa-calendar"></i>
                  </span>
                </span>
              </div>
            </div>
            <button class="btn btn-primary"><i class="fas fa-search"></i></button>
        </div>
    </form>
    <br>
    <div class="row justify-content-center">
        @if(count($posts) > 0)
            @foreach($posts as $post)
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title">{{$post->tittle}}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{$post->created_at}}</h6>
                            <p class="card-text">{{$post->description}}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <h5 class="text-center">no matches</<h5>
        @endif

    </div>
</div>
@section('scripts')
    <script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>
@endsection
@endsection