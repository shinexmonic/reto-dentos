@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Posts</div>

                <div class="card-body">

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                            @php
                                Session::forget('success');
                            @endphp
                        </div>
                    @endif

                    <a href="{{ route('post.create') }}" class="btn btn-primary"><i class="fas fa-plus"></i> Add</a>
                    <a type="submit" class="btn btn-info" href="{{ route('importPosts') }}"><i class="fas fa-rocket"></i> Consuming API</a>
                    <br>
                    <br>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Actions</th>
                                <th scope="col">Tittle</th>
                                <th scope="col">Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    <td>
                                        @if(auth()->user()->administrator == 1)
                                            <div style="display: table-cell;">
                                                <a class="btn btn-sm btn-warning" href="{{route('post.edit', $post->id)}}" style="display: table-cell;">
                                                    <i class="far fa-edit"></i>
                                                </a>
                                                <form action="{{route('post.destroy', $post->id)}}" method="POST" style="display: table-cell;">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-sm btn-danger deletepost">
                                                        <i class="fas fa-trash"></i>
                                                    </button>
                                                </form>
                                            </div>
                                        @else
                                            no actions
                                        @endif
                                    </td>
                                    <td>{{$post->tittle}}</td>
                                    <td>{{$post->description}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            $('.deletepost').click(function(e){
                e.preventDefault() // Don't post the form, unless confirmed
                if (confirm('Are you sure delete this?')) {
                    // Post the form
                    $(e.target).closest('form').submit() // Post the surrounding form
                } else {
                    alert('Why did you press cancel? You should have confirmed');
                }
            });
        });
    </script>
@endsection
@endsection
