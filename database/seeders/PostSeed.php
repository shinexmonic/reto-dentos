<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class PostSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
        	[
	            'tittle'      => 'Test 1',
				'description' => "Some quick example text to build on the card title and make up the bulk of the card's content.",
				'user_id'     => 1,
				'created_at'  => date('Y-m-d H:i:s')
        	],
        	[
	            'tittle'      => 'Test 2',
				'description' => "Some quick example text to build on the card title and make up the bulk of the card's content.",
				'user_id'     => 1,
				'created_at'  => date('Y-m-d H:i:s')
        	],
        	[
	            'tittle'      => 'Test 3',
				'description' => "Some quick example text to build on the card title and make up the bulk of the card's content.",
				'user_id'     => 1,
				'created_at'  => date('Y-m-d H:i:s')
        	]
        ]);
    }
}
