<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Faker\Factory;
use GuzzleHttp\Client;
use URL;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['getPostsApi']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('user_id', Auth::user()->id)->get();
        return view('post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tittle' => ['required', 'string', 'max:100'],
            'description' => ['required', 'string', 'max:500'],
        ]);

        Post::create([
            'tittle'       => $request->tittle,
            'description' => $request->description,
            'user_id'     => Auth::user()->id
        ]);

        return redirect()->route('post.index')->with('success', 'Posts created successfully.');;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $post = Post::where('id', $post->id)->first();

        return view('post.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $request->validate([
            'tittle' => ['required', 'string', 'max:100'],
            'description' => ['required', 'string', 'max:500'],
        ]);

        Post::where('id', $post->id)->update([
            'tittle' => $request->tittle,
            'description' => $request->description,
        ]);

        return redirect()->route('post.index')->with('success', 'Posts updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect()->route('post.index')->with('success', 'Posts deleted successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function import(Post $post)
    {
        $client = new Client();
        $url = URL::to('/');
        $response = $client->request('GET', $url.'/get/posts/'.Auth::user()->id);
        $response = (array)json_decode($response->getBody()->getContents());

        if(count($response)>0)
        {
            foreach ($response as $resp) 
            {
                Post::create((array)$resp);
            }

            return redirect()->route('post.index')->with('success', 'Posts added successfully.');
        }
        else
        {
            return redirect()->route('post.index')->with('danger', 'There are no posts!');
        }
    }


    public function getPostsApi(int $userId)
    {
        $faker = Factory::create();

        $arraySend = [];

        for ($i=0; $i <= 30 ; $i++) 
        {
            $arraySend[] = [
                'tittle'      => $faker->text,
                'description' => $faker->paragraph,
                'user_id'     => $userId
            ];
        }
        return $arraySend;
    }
}
