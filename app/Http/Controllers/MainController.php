<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
    public function index()
    {
    	$posts = Post::orderBy('id');

    	if(isset($_GET['created_at']))
    	{
	    	$originalDate = $_GET['created_at'];
			$newDate = date("Y-m-d", strtotime($originalDate) );

	    	$posts = Post::whereDate('created_at', $newDate);
    	}

    	$posts = $posts->get();

    	return view('publicconsultation', compact('posts'));
    }

    public function getPostsDate(Request $request)
    {
    	return view('publicconsultation', compact('posts'));
    }

}
